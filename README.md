# EOSContainerCondor

Demonstrator to show different behavior when using EOS in dockeruniverse and in singularity on CERNs batch system.

## Running both examples:

```
mkdir output
condor_submit docker.sub
condor_submit singularity.sub
```

## Result of singularity job
`.out`:
```
Running in singularity
/tmp/merrenst.cc
Ticket cache: FILE:/tmp/merrenst.cc
Default principal: merrenst@CERN.CH

Valid starting     Expires            Service principal
03/21/19 10:38:24  03/22/19 11:38:24  krbtgt/CERN.CH@CERN.CH
renew until 03/26/19 09:28:57
03/21/19 11:20:19  03/22/19 11:38:24  afs/cern.ch@CERN.CH
renew until 03/26/19 09:28:57
Configured GCC from: /opt/lcg/gcc/6.2.0binutils/x86_64-slc6
Configured AnalysisBase from: /usr/AnalysisBase/21.2.59/InstallArea/x86_64-slc6-gcc62-opt
Path:   /eos/atlas/atlasgroupdisk
Id:     1351931150781183
Size:   474480992694815
MTime:  2019-03-13 15:09:55
Flags:  51 (XBitSet|IsDir|IsReadable|IsWritable)
```

## Result of docker job
`.out`:
```
Running in docker
FILE:/pool/condor/dir_30703/merrenst.cc
Ticket cache: FILE:/pool/condor/dir_30703/merrenst.cc
Default principal: merrenst@CERN.CH

Valid starting     Expires            Service principal
03/21/19 10:38:24  03/22/19 11:38:24  krbtgt/CERN.CH@CERN.CH
renew until 03/26/19 09:28:57
03/21/19 11:24:36  03/22/19 11:38:24  afs/cern.ch@CERN.CH
renew until 03/26/19 09:28:57
Configured GCC from: /opt/lcg/gcc/6.2.0binutils/x86_64-slc6
Configured AnalysisBase from: /usr/AnalysisBase/21.2.59/InstallArea/x86_64-slc6-gcc62-opt
```

With the error in `.err`:
```
[FATAL] Invalid address
```

I also observed some docker jobs that just appear to be stuck at the `xrdfs` command instead.
