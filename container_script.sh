#!/bin/bash
echo "Running in $1"

# Debugging output
if [ $1 = "singularity" ]; then
   export KRB5CCNAME="$PWD/merrenst.cc"
fi
echo $KRB5CCNAME
klist

source /home/atlas/release_setup.sh

xrdfs eosatlas.cern.ch stat /eos/atlas/atlasgroupdisk
