#!/bin/sh
export SINGULARITY_CACHEDIR=$PWD/.singularity
singularity exec --no-home -B $PWD:/tmp --pwd /tmp -C docker://atlrpv1l/stop1lxaod:condor-container /tmp/container_script.sh singularity
